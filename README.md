Confidence intervals for a ratio
================

Discuss alternative methods for computing confidence interval for highly skewed data
like a ratio of variables.

Specifically, consider the dog-human ratio in a survey of households in Madagascar
for a study of rabies.


- [Report](https://umr-astre.pages.mia.inra.fr/confint_ratio/confint_ratio.html)



